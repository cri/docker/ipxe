FROM ubuntu:20.04

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
    && apt-get install -y \
       gcc \
       make \
       git \
       zlib1g-dev \
       liblzma-dev \
       mkisofs \
       isolinux
